package expression;

public class Main {
    public static void main(String[] args ) throws ExpressionError {
        Expression expr = new StackExpression();
        Integer result = expr.evaluate(args[0]);
        System.out.print(result);
    }
}
