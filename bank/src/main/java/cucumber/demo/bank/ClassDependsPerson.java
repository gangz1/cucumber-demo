package cucumber.demo.bank;

public class ClassDependsPerson {
    private final Person person;

    public ClassDependsPerson(Person person){
        this.person = person;
    }

    public String getName(){
        return person.getName();
    }
}
