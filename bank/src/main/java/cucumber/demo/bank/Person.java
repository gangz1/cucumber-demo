package cucumber.demo.bank;

import lombok.Data;

@Data
public class Person {
	public String orderNumber;
	public String name;
	public int age;
	public int height;
}
