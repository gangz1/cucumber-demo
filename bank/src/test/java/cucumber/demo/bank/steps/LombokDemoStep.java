package cucumber.demo.bank.steps;

import cucumber.demo.bank.ClassDependsPerson;
import cucumber.demo.bank.Person;
import io.cucumber.java.zh_cn.假如;
import org.mockito.Mockito;

import static org.junit.Assert.*;

public class LombokDemoStep {

    @假如("演示lombok的Data类被依赖的情况")
    public void 演示lombok的_data类被依赖的情况() {
        Person person = Mockito.mock(Person.class);
        Mockito.when(person.getName()).thenReturn("zhangsan");
        ClassDependsPerson dep = new ClassDependsPerson(person);
        assertEquals("zhangsan", dep.getName());
    }
}
