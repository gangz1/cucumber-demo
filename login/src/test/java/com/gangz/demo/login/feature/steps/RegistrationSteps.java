package com.gangz.demo.login.feature.steps;

import com.gangz.demo.login.register.RegistrationDto;
import io.cucumber.java.zh_cn.*;
import org.springframework.beans.factory.annotation.Autowired;

import java.net.http.HttpResponse;

import static org.junit.Assert.assertEquals;

public class RegistrationSteps {
    @Autowired HttpClientWrapper httpClient;
    RegistrationDto registrationDto = new RegistrationDto();
    private HttpResponse<String> result;

    @假如("用户名为{string}")
    public void setUsername(String username) {
        registrationDto = new RegistrationDto();
        registrationDto.setUsername(username);
    }
    @假如("密码为{string}")
    public void setPassword(String password) {
        registrationDto.setPassword(password);
    }
    @当("用户注册")
    public void registration() {
        result = httpClient.post("/register",registrationDto);
    }
    @那么("结果应该是{string}")
    public void verify_result(String result) {
        assertEquals(result,this.result.body());
    }
}
