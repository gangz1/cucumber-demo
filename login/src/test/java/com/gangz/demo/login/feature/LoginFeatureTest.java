package com.gangz.demo.login.feature;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
		features = "src/test/resources/com/gangz/demo/login",
		plugin = {"pretty","html:target/cucumber-html-report",
		"json:target/cucumber.json", "pretty:target/cucumber-pretty.txt","usage:target/cucumber-usage.json", "junit:target/cucumber-results.xml"},
		extraGlue = {"com.gangz.demo.login.config","com.gangz.demo.login.steps"})
class LoginFeatureTest {
	@Test
	void test() {
	}
}
