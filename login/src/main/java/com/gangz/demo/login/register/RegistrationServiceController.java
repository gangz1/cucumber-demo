package com.gangz.demo.login.register;

import com.gangz.demo.login.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class RegistrationServiceController {
   @Autowired
   UserService userService;

   @RequestMapping(value = "/register", method = RequestMethod.POST)
   public ResponseEntity<Object> register(@RequestBody RegistrationDto registrationDto) {
      Long result = userService.register(registrationDto);
      if (result==null){
         return new ResponseEntity<>("失败：无效密码",HttpStatus.BAD_REQUEST);
      }else {
         return new ResponseEntity<>("注册成功", HttpStatus.OK);
      }
   }

}