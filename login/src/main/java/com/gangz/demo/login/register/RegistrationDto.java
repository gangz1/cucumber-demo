package com.gangz.demo.login.register;

import lombok.Data;

@Data
public class RegistrationDto {
    private String password;
    private String username;
    public  RegistrationDto(){
    }

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    public String getUsername(){
        return username;
    }
}
