package com.gangz.demo.login.user;
import jakarta.persistence.*;
import lombok.Data;

@Entity
@Data
@Table(name = "TUser")
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    Long id;
    String name;
    String password;
}
