package com.gangz.demo.login.register;

import com.gangz.demo.login.UserRepository;
import com.gangz.demo.login.user.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {
    @Autowired
    UserRepository repo;
    public Long register(RegistrationDto registrationDto) {
        if (!PasswordValidator.checkPassword(registrationDto.getPassword())) return null;
        User user = new User();
        user.setId(1L);
        repo.save(user);
        return user.getId();
    }

    public User retrieveUser(Long userId) {
        return repo.findById(userId).get();
    }
}
