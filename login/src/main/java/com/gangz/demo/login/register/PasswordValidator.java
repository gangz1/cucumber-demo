package com.gangz.demo.login.register;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class PasswordValidator {
    public static boolean checkPassword(String password){
        String passwordPattern = "^(?=.*[0-9])(?=.*[A-Za-z])(?=.*[!@#&()–[{}]:;',?/*~$^+=<>]).{8,}$";
        Pattern Password_Pattern = Pattern.compile(passwordPattern);
        Matcher matcher = Password_Pattern.matcher(password);
        if (matcher.matches()) {
            return true;
        }
        return false;
    }
}
