docker run \
  --name local-mysql \
  -p 3306:3306 \
  -e MYSQL_ROOT_PASSWORD=p4ssw0rd \
  -e MYSQL_ROOT_HOST='%' \
  -d mysql/mysql-server:8.0

#docker exec -it local-mysql bash
#
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' local-mysql
172.17.0.2